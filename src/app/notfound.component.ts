import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'notfound',
    templateUrl: 'notfound.component.html'
})

export class NotFoundComponent implements OnInit {
    title = 'Not Found';

    constructor() { }

    ngOnInit() { }
}
