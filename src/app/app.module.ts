import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { UIRouterModule, UIRouter } from '@uirouter/angular';

import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import { AboutComponent } from './about.component';
import { NotFoundComponent } from './notfound.component';

// define states -------
const defaultState = {
  name: 'home',
  url: '/home',
  component: HomeComponent
};

const aboutState = {
  name: 'about',
  url: '/about',
  component: AboutComponent
};

const notFoundState = {
  name: 'notfound',
  url: '/notfound',
  component: NotFoundComponent
};

export function uiRouterConfigFn(router: UIRouter, injector: Injector) {
  router.urlService.rules.initial({state: defaultState.name});
  router.urlService.rules.otherwise({state: notFoundState.name});
}

@NgModule({
  declarations: [
    AppComponent, HomeComponent, AboutComponent, NotFoundComponent
  ],
  imports: [
    BrowserModule,
    UIRouterModule.forRoot({
      states: [defaultState, aboutState, notFoundState],
      useHash: true,
      config: uiRouterConfigFn
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
